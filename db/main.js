const mongoose = require('mongoose');
const painting = require('./models/painting');
require('dotenv').config();
// const DATABASE_URL = "mongodb://localhost:27017/art"
const DATABASE_URL = process.env["MONGO_SERVER"]
console.log("DATABASE_URL", DATABASE_URL)
mongoose.connect(DATABASE_URL, {useNewUrlParser: true});
const conn = mongoose.connection;
conn.on('error', console.error.bind(console, 'connection error:'));
conn.once('open', function() {
  console.log("db connected")
});

// mongoose.Promise = global.Promise;

const Painting = mongoose.model('painting', painting);

module.exports = {
  conn,
  Painting,
}
