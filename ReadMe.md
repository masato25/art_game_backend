## openvino互動頁面
![](demo_img2.png)
### 安裝及設定
#### step 1
啟動mongodb
ps. docker 環境安裝請參照網路
```
docker run -p 27017:27017 --name mymongo -d mongo
```
#### step 2
clone 相關環境
```
mkdir artweb
cd artweb
export artweb=$PWD
git clone git@gitlab.com:masato25/art_game_backend.git
git clone git@gitlab.com:masato25/art_game_frontend.git
```
需求環境:
* node v10.15.3 以上
* npm 6.4.1 以上

```
npm i -g yarn
```
安裝dependents
```
cd $artweb/art_game_backend
yarn
cd $artweb/art_game_frontend
yarn
```
建立資料庫data
* require: ruby 2.6.0以上

```
cd $artweb/art_game_backend/csv_helper
gem install bundler
bundle
ruby rhelp.rb
```
#### step 3
啟動
```
#terminal1
cd $artweb/art_game_backend
node index.js
# listen port : 3001
#terminal2
cd $artweb/art_game_frontend
npm run serve
# -> open localhost:8080
```

* 關於運作資訊可以參考 `mock_explain.md`
