require 'csv'
require 'mongo'

# read data from Met meta
arr_of_rows = CSV.read("MetObjects.csv.Paintings_add_header.csv", {headers: true})

artmap = {}
arr_of_rows.each do |o|
  obj = o.to_h
  artmap[obj["id"]] = obj
end

# read data from MET_angle
arr_of_rows = CSV.read("MET_angle.csv", {headers: true})

anglemap = {}
arr_of_rows.each do |o|
  obj = o.to_h
  anglemap[obj["ID"]] = obj
end

client = Mongo::Client.new([ 'mymongo:27017' ], :database => 'art')
collection = client[:paintings]

files = Dir["../public/selected/*.jpg"]
fsize = files.size
files.each_with_index do |file, indx|
  querykeyid = file.gsub("../public/selected/", "").gsub(".json.jpg", "")
  matched = artmap[querykeyid]
  if matched
    matchedAng = anglemap[querykeyid+".json"]
    record = {}
    record["id"] = matched["id"]
    record["name"] = matched["name"]
    record["country"] = matched["country"]
    record["star_year"] = matched["star_year"]
    record["end_year"] = matched["end_year"]
    record["museum"] = matched["museum"]
    record["painter"] = matched["painter"]
    record["url"] = matched["url"]
    record["filename"] = file.gsub("../public/selected/", "")
    if matchedAng == nil
      puts "@@@@@@@"
      p record
    end
    # 上下
    record["pitch"] = matchedAng["angle_p_fc"].to_f
    # 左右
    # record["yaw"] = (180 / fsize) * indx
    record["yaw"] = matchedAng["angle_y_fc"].to_f
    # 傾斜度
    record["roll"] = matchedAng["angle_r_fc"].to_f
    p record
    collection.insert_one(record)
  else
    puts "@@@#{file} not found"
  end
end
